package com.ygsoft.nagent.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.ygsoft.nagent.constant.AgentConstant;

public class PropertiesUtil {

	private Properties agentPro = new Properties();
	
	private PropertiesUtil() {
		init();
	}
	
	public static PropertiesUtil getInstance() {
		return Singleton.INSTANCE;
	}
	
	private static class Singleton {
		private static final PropertiesUtil INSTANCE = new PropertiesUtil();
	}
	
	private void init() {
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(AgentConstant.AGENT_PROPERTIES_NAME);
		if(in != null) {
			try {
				agentPro.load(in);
			} catch (IOException e) {
				e.printStackTrace();
			}finally {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public String getAgentPro(String key) {
		return agentPro.getProperty(key);
	}
	
	public static void main(String[] args) {
		PropertiesUtil.getInstance();
	}
}
