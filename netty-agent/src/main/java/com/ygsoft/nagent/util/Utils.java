package com.ygsoft.nagent.util;

import java.lang.reflect.Field;
import java.util.Vector;

public class Utils {

	public static void printClasses(ClassLoader classLoader) {
		try {
			Class cla = classLoader.getClass() ;
			while(cla != ClassLoader.class) {
				cla = cla.getSuperclass();
			}
			Field field;
			field = cla.getDeclaredField("classes");
			field.setAccessible(true);
			Vector  v = (Vector) field.get(classLoader);
			for(int i = 0 ; i < v.size(); i ++) {
				String className = ((Class)v.get(i)).getName() ;
				System.err.println("------------------->"+((Class)v.get(i)).getClassLoader()+"   "+ ((Class)v.get(i)).getName());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void printPreByDeep(String pre , int deep ) {
		for(int i = 0 ; i < deep ; i ++) {
			System.out.print("   ");
		}
		System.out.print(pre);
	}
}
