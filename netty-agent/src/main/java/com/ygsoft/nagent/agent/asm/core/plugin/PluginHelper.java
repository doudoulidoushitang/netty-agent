package com.ygsoft.nagent.agent.asm.core.plugin;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ServiceLoader;
import java.util.Set;

import com.ygsoft.nagent.agent.asm.core.service.IPlugin;

public class PluginHelper {

	public PluginHelper() {
		loadService();
	}
	
	private Set<IPlugin> plugins = new HashSet<>();
	
	public synchronized List<IPlugin> matchClassName(String className) {
		LinkedList<IPlugin> result = new LinkedList<>();
		for(IPlugin plugin : plugins) {
			if(plugin.match(className)) {
				result.add(plugin) ;
			}
		}
		return result;
	}
	
	private void loadService() {
		Iterator<IPlugin> ppp = ServiceLoader.load(IPlugin.class).iterator();
		while(ppp.hasNext()) {
			IPlugin plugin = ppp.next();
			plugins.add(plugin);
		}
	}
	
	public static void main(String[] args) {
	}
}
