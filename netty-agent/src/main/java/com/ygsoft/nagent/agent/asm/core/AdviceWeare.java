package com.ygsoft.nagent.agent.asm.core;

import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.AdviceAdapter;
import org.objectweb.asm.commons.JSRInlinerAdapter;
import org.objectweb.asm.commons.Method;

import com.ygsoft.nagent.agent.asm.core.service.AdviceListener;
import com.ygsoft.nagent.util.Utils;

public class AdviceWeare extends ClassVisitor implements Opcodes{

	private String className ;
	
	private AdviceListener listener ;
	
	private Class<?> targetClazz ;
	
	private int adviceId ;
	
	private ClassWriter writer ;
	
	private ClassLoader classLoader ; 
	
	/****
	 * 构造方法，传入listener
	 * @param className
	 * @param listener
	 * @param targetClazz
	 * @param adviceId
	 */
	public AdviceWeare(String className , AdviceListener listener , ClassLoader classLoader ,ClassVisitor cv) {
		super(Opcodes.ASM7,cv);
		writer = (ClassWriter) super.cv ;
		this.className = className ; 
		this.listener = listener ;
		this.classLoader = classLoader ;
		this.adviceId = adviceidProducer.getAndIncrement() ;
		LISTENER_MAP.put(this.adviceId, this.listener);
	}

	private static AtomicInteger adviceidProducer = new AtomicInteger(1);
	
	private static final ConcurrentHashMap<Integer, AdviceListener> LISTENER_MAP = new ConcurrentHashMap<>();
	
	private static final ThreadLocal<AtomicInteger> methodDeep = new ThreadLocal<AtomicInteger>() {
		
		@Override
		protected AtomicInteger initialValue() {
			return new AtomicInteger(0) ;
		}
	};
	
	private static final ThreadLocal<Boolean> selfCall = new ThreadLocal<Boolean>() {
		
		@Override
		protected Boolean initialValue() {
			return false ;
		}
	};
	
	private static final ThreadLocal<Stack<Stack<Object>>> THREAD_STACK = new ThreadLocal<Stack<Stack<Object>>>() {
		@Override
		protected Stack<Stack<Object>> initialValue() {
			return new Stack<>() ;
		}
	};
	
	public static void methodOnBefore(int adviceId , ClassLoader classLoader, String className, String methodName, String methodDesc, Object target,
            Object[] args) {
		if(selfCall.get()) {
			return ;
		}
		selfCall.set(true);
		int deep = methodDeep.get().incrementAndGet();
		try {
			AdviceListener listener = LISTENER_MAP.get(adviceId);
			Stack<Object> stack = new Stack<>();
			stack.push(listener);
			stack.push(classLoader);
			stack.push(className);
			stack.push(methodName);
			stack.push(methodDesc);
			stack.push(target);
			stack.push(args);
			stack.push(System.currentTimeMillis());
			THREAD_STACK.get().push(stack);
			if(listener != null) {
				try {
					listener.before(deep ,classLoader, className, methodName, methodDesc, target, args);
				} catch (Throwable e) {
					//do something
				}
			}
		} finally {
			selfCall.set(false);
		}
	}
	
	 public static void methodOnReturning(Object returnVal) {
	        methodOnEnd(false, returnVal);
	    }

	    public static void methodOnThrowing(Throwable throwable) {
	        methodOnEnd(true, throwable);
	    }

	    public static void methodOnEnd(boolean isThrowing, Object returnTarget) {
	        if (selfCall.get()) {
	            return;
	        } else {
	        	selfCall.set(true);
	        }
	        int deep = methodDeep.get().getAndDecrement();
	        Long nowTime = System.currentTimeMillis();
	        //从栈中恢复执行现场
	        try {
	            Stack<Object> beginMethodFrame = THREAD_STACK.get().pop();
	            Long startTime = (Long)beginMethodFrame.pop();
	            Object[] args = (Object[])beginMethodFrame.pop();
	            Object target = beginMethodFrame.pop();
	            String methodDesc = (String)beginMethodFrame.pop();
	            String methodName = (String)beginMethodFrame.pop();
	            String className = (String)beginMethodFrame.pop();
	            ClassLoader classLoader = (ClassLoader)beginMethodFrame.pop();
	            AdviceListener listener = (AdviceListener)beginMethodFrame.pop();
	            try {
	            	if (isThrowing) {
		                //执行
	            		listener.afterThrowing(deep,classLoader, className, methodName, methodDesc, target, args, (Throwable)returnTarget ,(nowTime-startTime));
		            } else {
		            	listener.afterReturning(deep , classLoader, className, methodName, methodDesc, target, args, returnTarget,(nowTime-startTime));
		            }
				} catch (Throwable e) {
					//do something
				}
	        } finally {
	            selfCall.set(false);
	        }
	    }
	
	/***
	 * 方法权限
	 * 方法名，
	 * 方法描述
	 * 方法签名
	 * throws
	 */
	@Override
	public MethodVisitor visitMethod(final int access, final String name, final String descriptor, 
			final String signature, final String[] exceptions) {
		MethodVisitor methodVisitor = super.visitMethod(access, name, descriptor, signature, exceptions);
		if(methodVisitor == null ) {
			return null ; 
		}
		if(listener.isIgnore(access, classLoader , className , name)) {
			return methodVisitor ;
		}
		System.out.println("增强:"+className+"   "+name);
		return new AdviceAdapter(ASM5, new JSRInlinerAdapter(methodVisitor, access, name, descriptor, signature, exceptions),
	            access, name, descriptor) {
			
			private Label startLable = new Label() ;
			
			private Label endLable = new Label();
			
			@Override
			protected void onMethodEnter() {
				/**将adviceid压入堆栈***/
				push(adviceId);
				/**将adviceid转换为int**/
				box(Type.getType(Integer.class));
				loadClassLoader();
				push(className);
				push(name);
				push(descriptor);
				if((ACC_STATIC & methodAccess) == ACC_STATIC) {
					push((Type)null);
				}else {
					loadThis();
				}
				loadArgArray();
				invokeStatic(Type.getType(AdviceWeare.class), MethodFinder.getAsmMethod(AdviceWeare.class, "methodOnBefore",
						int.class, ClassLoader.class, String.class, String.class, String.class, Object.class,
			            Object[].class));
				mark(startLable);
			}
			
			@Override
			protected void onMethodExit(final int opcode) {
				 //判断不是以一场结束
                if (ATHROW != opcode) {
                    //加载正常的返回值
                    loadReturn(opcode);
                    //只有一个参数就是返回值
                    invokeStatic(Type.getType(AdviceWeare.class), MethodFinder.getAsmMethod(AdviceWeare.class, "methodOnReturning",	Object.class));
                }
			}
			
			@Override
	        public void visitMaxs(int maxStack, int maxLocals) {
				//每个方法最后调用一次,在visitEnd之前
                mark(endLable);
                //在beginLabel和endLabel之间使用try-catch block,在这之后需要紧跟exception的处理逻辑code
                catchException(startLable, endLable, Type.getType(Throwable.class));
                //从栈顶加载异常(复制一份给onThrowing当参数用)
                dup();
                invokeStatic(Type.getType(AdviceWeare.class), MethodFinder.getAsmMethod(AdviceWeare.class, "methodOnThrowing",	Throwable.class));
                //将原有的异常抛出(不破坏原有异常逻辑)
                throwException();
                super.visitMaxs(maxStack, maxLocals);
			}
			
			private void loadClassLoader() {
				if((ACC_STATIC & methodAccess) == ACC_STATIC) {
					visitLdcInsn(className.replaceAll("/", "."));
					invokeStatic(Type.getType(Class.class), MethodFinder.getAsmMethod(Class.class, "forName", String.class));
				}else {
					loadThis();
					invokeVirtual(Type.getType(Object.class), MethodFinder.getAsmMethod(Object.class, "getClass"));
				}
				invokeVirtual(Type.getType(Class.class), MethodFinder.getAsmMethod(Class.class, "getClassLoader"));
			}
			
			private void loadReturn(int opcode) {
                switch (opcode) {

                    case RETURN: {
                        push((Type)null);
                        break;
                    }

                    case ARETURN: {
                        dup();
                        break;
                    }

                    case LRETURN:
                    case DRETURN: {
                        dup2();
                        box(Type.getReturnType(methodDesc));
                        break;
                    }

                    default: {
                        dup();
                        box(Type.getReturnType(methodDesc));
                        break;
                    }

                }
            }
			
		};
	}
	static class MethodFinder {

        private MethodFinder() {

        }

        static Method getAsmMethod(final Class<?> clazz, final String methodName, final Class<?>... parameterTypes) {
            return Method.getMethod(getJavaMethodUnsafe(clazz, methodName, parameterTypes));
        }

        static java.lang.reflect.Method getJavaMethodUnsafe(final Class<?> clazz, final String methodName,
                                                            final Class<?>... parameterTypes) {
            try {
                return clazz.getDeclaredMethod(methodName, parameterTypes);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        }
    }
	public ClassWriter getWriter() {
		return writer;
	}

	public void setWriter(ClassWriter writer) {
		this.writer = writer;
	}
	
}
