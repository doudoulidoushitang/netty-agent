package com.ygsoft.nagent.agent.asm.core.service;

public interface IPlugin {

	boolean match(String className);
	
	byte[] enhance(ClassLoader laoder, String className , byte[] classfileBuffer);
	
	AdviceListener getListener();
	
}
