package com.ygsoft.nagent.agent.asm.core;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.List;

import com.ygsoft.nagent.agent.asm.core.plugin.PluginHelper;
import com.ygsoft.nagent.agent.asm.core.service.IPlugin;
import com.ygsoft.nagent.util.Utils;

public class MyTransFormer implements ClassFileTransformer {

	private PluginHelper pluginHelper ;
	
	public MyTransFormer(PluginHelper helper) {
		this.pluginHelper = helper ;
	}
	
	public byte[] transform(ClassLoader classLoader, String className, Class<?> classBeingRedefined,
			ProtectionDomain protectionDomain, byte[] classfileBuffer)
			throws IllegalClassFormatException {
		if(className == null){
			return classfileBuffer ;
		}
		List<IPlugin> plugins = pluginHelper.matchClassName(className);
		
		for(IPlugin plugin : plugins) {
			Utils.printClasses(classLoader);
			classfileBuffer  = plugin.enhance(classLoader ,className , classfileBuffer);
			storeClassFile(classfileBuffer, className);
		}
		return classfileBuffer;
	}
	
	private static void storeClassFile(byte[] bytes, String className) {
		OutputStream outputStream = null;
		try {
			File classFile = new File("D:\\data\\" + "classes" + File.separator + className.replace("/", File.separator) + ".class");
			if (!classFile.getParentFile().exists()) {
				classFile.getParentFile().mkdirs();
			}
			classFile.createNewFile();
			outputStream= new FileOutputStream(classFile);
			outputStream.write(bytes);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("持久化Class失败: " + className);
		} finally {
			if(outputStream!=null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
}
