package com.ygsoft.nagent.agent.plugin.method;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class MyProfiler {

	public static AtomicInteger methodIdGrower = new AtomicInteger(1);
	
	public static HashMap<String , MethodCache> methodCache = new HashMap<String , MethodCache>();
	
	public static LinkedList<MethodCache> stack = new LinkedList<MethodCache>();
	
	public static void setCache(String methodId,MethodCache cache) {
		if(cache == null) {
			cache = new MethodCache();
		}
		methodCache.put(methodId,cache);
	}
	
	public static void startMethod(String methodId) {
		MethodCache cache = methodCache.get(methodId);
		cache.setStartTime(System.currentTimeMillis());
		methodCache.put(methodId ,cache);
		stack.addLast(cache);
	}
	
	public static void methodEnd(String methodId) {
	/*	MethodCache cache = methodCache.get();
		cache.setEndTime(System.currentTimeMillis());
		methodCache.set(cache);*/
		MethodCache cache = stack.removeLast();
		cache.setEndTime(System.currentTimeMillis());
		
		System.out.println("方法执行信息："+cache.getClassName()+" -> "+cache.getMethodName() +" 方法耗时:"+((cache.getEndTime()-cache.getStartTime())/1000.0)+"s" );
	}
	
	public static Throwable methodError(String methodId,Throwable error)throws Throwable {
		MethodCache cache = methodCache.get(methodId);
		cache.setEndTime(System.currentTimeMillis());
		methodCache.get(methodId).setError(error);
		System.out.println("方法执行信息："+cache.getClassName()+" -> "+cache.getMethodName() +" 方法耗时:"+((cache.getEndTime()-cache.getStartTime())/1000.0)+"s" );
		System.out.println("方法报错 -> "+error.getMessage());
		return error ;
	}
}
