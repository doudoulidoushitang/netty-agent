package com.ygsoft.nagent.agent.asm.core;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import com.ygsoft.nagent.agent.asm.core.service.AbstractClassAdapter;

public class MyDemoAdapter extends AbstractClassAdapter{

	private String className ;
	
	private boolean isInterface ;
	
	private boolean isEnum ;
	
	private boolean isAnnoation ;
	
	public MyDemoAdapter(String className) {
		super();
		this.className = className ;
	}

	@Override
	public void visit(
		      final int version,
		      final int access,
		      final String name,
		      final String signature,
		      final String superName,
		      final String[] interfaces) {
	      super.visit(version, access, name, signature, superName, interfaces);
	    this.isInterface = (access & Opcodes.ACC_INTERFACE) > 0 ;
	    this.isEnum = (access & Opcodes.ACC_ENUM) > 0 ;
	    this.isAnnoation = (access & Opcodes.ACC_ANNOTATION) > 0;
	    
	}

	@Override
	public MethodVisitor visitMethod(
		      final int access,
		      final String name,
		      final String descriptor,
		      final String signature,
		      final String[] exceptions) {
		MethodVisitor methodVisitor = super.visitMethod(access, name, descriptor, signature, exceptions);
		if("sayHello".equals(name)||"printMsg".equals(name)) {
			return new MyMethodAdapter(methodVisitor , name , className);
		}
		return methodVisitor;
	}
	
	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public boolean isInterface() {
		return isInterface;
	}

	public void setInterface(boolean isInterface) {
		this.isInterface = isInterface;
	}

	public boolean isEnum() {
		return isEnum;
	}

	public void setEnum(boolean isEnum) {
		this.isEnum = isEnum;
	}

	public boolean isAnnoation() {
		return isAnnoation;
	}

	public void setAnnoation(boolean isAnnoation) {
		this.isAnnoation = isAnnoation;
	}
	
}
