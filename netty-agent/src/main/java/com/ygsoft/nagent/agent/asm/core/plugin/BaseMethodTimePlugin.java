package com.ygsoft.nagent.agent.asm.core.plugin;

import java.util.Arrays;

import org.objectweb.asm.Opcodes;

import com.ygsoft.nagent.agent.AgentMain;
import com.ygsoft.nagent.agent.asm.core.service.AbstractEnhancer;
import com.ygsoft.nagent.agent.asm.core.service.AdviceListener;
import com.ygsoft.nagent.constant.AgentConstant;
import com.ygsoft.nagent.util.Utils;

public class BaseMethodTimePlugin extends AbstractEnhancer{

	@Override
	public String getMatchString() {
		return "com.sun.ygsoft.ecp.apm.test.";
	}

	@Override
	public AdviceListener getListener() {
		return new AdviceListener() {
			
			@Override
			public boolean isIgnore(int access,ClassLoader loader , String className, String methodName) {
				if((Opcodes.ACC_ABSTRACT & access) ==Opcodes. ACC_ABSTRACT
			            || (Opcodes.ACC_FINAL & access) == Opcodes.ACC_FINAL
			            || "<clinit>".equals(methodName)
			            || "<init>".equals(methodName)
						|| "main".equals(methodName)) {
					return true ;
				}
				if(loader == null) {
					return true ;
				}else {
					return !loader.equals(AgentMain.agentLoader().getParent());
				}
			}
			
			@Override
			public void before(int deep , ClassLoader classLoader, String className, String methodName, String methodDesc, Object target,
					Object[] args) throws Throwable {
				if(deep == 1) {
					System.out.println(AgentConstant.STARTINVOKE_SEP);	
				}
				Utils.printPreByDeep(AgentConstant.methodSep, deep);
				System.out.println(methodName+"("+Arrays.toString(args)+")");
//				System.out.println("开始执行方法 "+methodName +" 方法参数:"+Arrays.toString(args));
			}
			
			@Override
			public void afterThrowing(int deep , ClassLoader loader, String className, String methodName, String methodDesc, Object target,
					Object[] args, Throwable throwable , Long costTime) throws Throwable {
				Utils.printPreByDeep(AgentConstant.methodSep, deep);
				System.out.println(costTime+"  "+methodName+" throws "+throwable.getMessage());
				if(deep == 1) {
					System.out.println(AgentConstant.ENDINVOKE_SEP);
				}
//				System.out.println("执行方法报错 "+methodName +" 方法参数:"+Arrays.toString(args)+" 总耗时："+costTime+" 错误信息:"+throwable.getMessage());
			}
			
			@Override
			public void afterReturning(int deep , ClassLoader classLoader, String className, String methodName, String methodDesc,
					Object target, Object[] args, Object returnObj, Long costTime) throws Throwable {
				Utils.printPreByDeep(AgentConstant.methodSep, deep);
				System.out.println(costTime+"  "+methodName+" return "+returnObj);
				if(deep == 1) {
					System.out.println(AgentConstant.ENDINVOKE_SEP);
				}
//				System.out.println("方法执行结束 "+methodName +" 方法参数:"+Arrays.toString(args)+" 总耗时："+costTime);
			}
		};
	}

}
