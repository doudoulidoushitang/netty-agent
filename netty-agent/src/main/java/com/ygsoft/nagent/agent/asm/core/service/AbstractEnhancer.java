package com.ygsoft.nagent.agent.asm.core.service;

import static org.objectweb.asm.ClassWriter.COMPUTE_FRAMES;
import static org.objectweb.asm.ClassWriter.COMPUTE_MAXS;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;

import com.ygsoft.nagent.agent.asm.core.AdviceWeare;

public abstract class AbstractEnhancer implements IPlugin{

	public byte[] enhance(ClassLoader loader ,String className , byte[] classfileBuffer) {
		 ClassReader cr;
		try {
			cr = new ClassReader(classfileBuffer);
			final ClassWriter cw = new ClassWriter(cr, COMPUTE_FRAMES | COMPUTE_MAXS) {
	            /*
	             * 注意，为了自动计算帧的大小，有时必须计算两个类共同的父类。
	             * 缺省情况下，ClassWriter将会在getCommonSuperClass方法中计算这些，通过在加载这两个类进入虚拟机时，使用反射API来计算。
	             * 但是，如果你将要生成的几个类相互之间引用，这将会带来问题，因为引用的类可能还不存在。
	             * 在这种情况下，你可以重写getCommonSuperClass方法来解决这个问题。
	             *
	             * 通过重写 getCommonSuperClass() 方法，更正获取ClassLoader的方式，改成使用指定ClassLoader的方式进行。
	             * 规避了原有代码采用Object.class.getClassLoader()的方式
	             */
	            @Override
	            protected String getCommonSuperClass(String type1, String type2) {
	                Class<?> c, d;
	                try {
	                    c = Class.forName(type1.replace('/', '.'), false, loader);
	                    d = Class.forName(type2.replace('/', '.'), false, loader);
	                } catch (Exception e) {
	                    throw new RuntimeException(e);
	                }
	                if (c.isAssignableFrom(d)) {
	                    return type1;
	                }
	                if (d.isAssignableFrom(c)) {
	                    return type2;
	                }
	                if (c.isInterface() || d.isInterface()) {
	                    return "java/lang/Object";
	                } else {
	                    do {
	                        c = c.getSuperclass();
	                    } while (!c.isAssignableFrom(d));
	                    return c.getName().replace('.', '/');
	                }
	            }
			};
			cr.accept(new AdviceWeare( className, getListener() , loader, cw),
					ClassReader.EXPAND_FRAMES);
			final byte[] enhancerByteArray = cw.toByteArray() ;
			return enhancerByteArray;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return classfileBuffer;
	        // 字节码增强
		/*AdviceWeare adviceWeare = new AdviceWeare(className, getListener());
		ClassWriter writer = adviceWeare.getWriter();
		ClassReader reader = new ClassReader(classfileBuffer);
		reader.accept(adviceWeare, 0);
		return writer.toByteArray();*/
		
		/*AbstractClassAdapter classAdapter = getAdapter(className);
		ClassWriter writer = classAdapter.getClassWriter();
		ClassReader reader = new ClassReader(classfileBuffer);
		reader.accept(classAdapter, 0);
		return writer.toByteArray();*/
	}
	
	@Override
	public boolean match(String className) {
		if(className.replaceAll("/", ".").indexOf(getMatchString()) >= 0) {
			return true ;
		}
		return false;
	}
	
	public abstract String getMatchString();
}
