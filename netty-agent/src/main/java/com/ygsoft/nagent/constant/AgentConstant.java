package com.ygsoft.nagent.constant;

public class AgentConstant {

	public static final String AGENT_PROPERTIES_NAME="agent.properties";
	
	public static final String COLLECTOR_IP="collector-ip";
	
	public static final String COLLECTOR_PORT="collector-port";
	
	public static final String methodSep = "---" ;
	
	public static final String ENDINVOKE_SEP="************************end**************************" ;
	
	public static final String STARTINVOKE_SEP="************************start**************************" ;
}
