package com.ygsoft.nagent.netty;

import com.ygsoft.nagent.model.MethodModel;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class ClientHandler extends SimpleChannelInboundHandler<MethodModel>{

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, MethodModel msg) throws Exception {
		//todo
	}
	
	@Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        System.out.println("链接已建立");
    }

    /**
     * Do nothing by default, sub-classes may override this method.
     */
    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        System.out.println("断开链接");
    }

}
