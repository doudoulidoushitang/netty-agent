package com.ygsoft.nagent.netty;

import java.io.Serializable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class NettySender extends NettyClient{

	private NettySender(){}
	
	/**线程池，也充当消息队列***/
	private ThreadPoolExecutor executor = new ThreadPoolExecutor(3, 3, 2, TimeUnit.MINUTES, new LinkedBlockingQueue<>(200));
	
	public static NettySender getInstance() {
		return Singleton.INSTANCE;
	}
	
	private static class Singleton{
		public static final NettySender INSTANCE = new NettySender();
	}
	
	public void sendMsg(Serializable msg) {
		executor.execute(()-> {
			if(getChannel() ==null) {
				reconnect();
			}
			getChannel().writeAndFlush(msg);
		});
	}
}
