package com.ygsoft.nagent.netty;

import org.springframework.util.SerializationUtils;

import com.ygsoft.nagent.model.MethodModel;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class AgentEncoder extends MessageToByteEncoder<MethodModel>{

	@Override
	protected void encode(ChannelHandlerContext ctx, MethodModel msg, ByteBuf out) throws Exception {
		byte[] data = SerializationUtils.serialize(msg);
		if(data != null && data.length > 0) {
			out.writeBytes(data);
		}
	}

}
