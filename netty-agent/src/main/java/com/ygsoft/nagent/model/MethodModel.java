package com.ygsoft.nagent.model;

import java.io.Serializable;

public class MethodModel implements Serializable{

	private String methodId ;
	
	private String className ;
	
	private String methodName ;
	
	private Long startNao ;
	
	private Long endNao ;
	
	private Throwable error ;
	
	private Object result ;
	
	private Long costTime ;

	public String getMethodId() {
		return methodId;
	}

	public void setMethodId(String methodId) {
		this.methodId = methodId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Long getStartNao() {
		return startNao;
	}

	public Long getCostTime() {
		return costTime;
	}

	public void setCostTime(Long costTime) {
		this.costTime = costTime;
	}

	public void setStartNao(Long startNao) {
		this.startNao = startNao;
	}

	public Long getEndNao() {
		return endNao;
	}

	public void setEndNao(Long endNao) {
		this.endNao = endNao;
	}

	public Throwable getError() {
		return error;
	}

	public void setError(Throwable error) {
		this.error = error;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "MethodModel [methodId=" + methodId + ", className=" + className + ", methodName=" + methodName
				+ ", result=" + result + ", costTime=" + costTime + "]";
	}
	
	
}
