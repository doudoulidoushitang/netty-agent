/*package com.ygsoft.nagent.service.imp;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ygsoft.nagent.elasticsearch.entity.EsMethodModel;
import com.ygsoft.nagent.elasticsearch.operate.repository.EsMethodRepository;
import com.ygsoft.nagent.service.EsMethodService;

@Service("esMethodService")
public class EsMethodServiceImp implements EsMethodService {

	@Autowired
	private EsMethodRepository esMethodService ;

	@Override
	public void saveMethod(EsMethodModel model) {
		if(model == null) {
			return ;
		}
		esMethodService.save(model);
	}

	@Override
	public void saveMethod(List<EsMethodModel> list) {
		if(list != null && list.size() > 0) {
			esMethodService.saveAll(list);
		}
	}

	@Override
	public List<EsMethodModel> searchByTraceId(String traceId) {
		if(StringUtils.isBlank(traceId)) {
			return Collections.EMPTY_LIST;
		}
		return esMethodService.findByTraceId(traceId);
	}

	@Override
	public List<EsMethodModel> searchByPSpanId(String pSpanId) {
		if(StringUtils.isBlank(pSpanId)) {
			return Collections.EMPTY_LIST;
		}
		return esMethodService.findByPSpanId(pSpanId);
	}
	
}
*/
