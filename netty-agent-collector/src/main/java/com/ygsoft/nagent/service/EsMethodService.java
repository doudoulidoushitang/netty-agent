package com.ygsoft.nagent.service;

import java.util.List;

import com.ygsoft.nagent.elasticsearch.entity.EsMethodModel;

public interface EsMethodService {

	void saveMethod(EsMethodModel model);
	
	void saveMethod(List<EsMethodModel> list);
	
	List<EsMethodModel> searchByTraceId(String traceId);
	
	List<EsMethodModel> searchByPSpanId(String pSpanId);
}
