package com.ygsoft.nagent.elasticsearch.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName="summary",type="method",indexStoreType="fs"
			,shards=1,replicas=1,refreshInterval="5")
public class EsMethodModel {

	private String traceId ;
	
	@Id
	private String spanId ; 
	
	private String pSpanId ;
	
	private String className ; 
	
	private String methodName ;
	
	private Long startNao ;
	
	private Long endNao ;
	
	private Long costTime ;

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	public String getSpanId() {
		return spanId;
	}

	public void setSpanId(String spanId) {
		this.spanId = spanId;
	}

	public String getpSpanId() {
		return pSpanId;
	}

	public void setpSpanId(String pSpanId) {
		this.pSpanId = pSpanId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Long getStartNao() {
		return startNao;
	}

	public void setStartNao(Long startNao) {
		this.startNao = startNao;
	}

	public Long getEndNao() {
		return endNao;
	}

	public void setEndNao(Long endNao) {
		this.endNao = endNao;
	}

	public Long getCostTime() {
		return costTime;
	}

	public void setCostTime(Long costTime) {
		this.costTime = costTime;
	}
}
