package com.ygsoft.nagent.config.command;

import java.util.concurrent.LinkedBlockingQueue;

import com.ygsoft.nagent.service.Task;

public class ThreadArrayExecutor {

	private int threadNum ;
	
	private Thread[] threads ;
	
	private LinkedBlockingQueue[] queues  ;
	
	private Task task ;
	
	public ThreadArrayExecutor(int threadnum , Task task) {
		if(threadnum < 1) {
			throw new RuntimeException("thread num cannot less than 1");
		}
		this.threadNum = threadnum ;
		init();
	}
	
	private void init() {
		threads = new Thread[threadNum];
		queues = new LinkedBlockingQueue[threadNum];
		for(int i = 0 ; i < threadNum ; i ++) {
			queues[i] = new LinkedBlockingQueue<>(200);
			threads[i] = new Thread(new Run(queues[i] , task));
			threads[i].start();
		}
	}
	
	class Run implements Runnable{
		
		private LinkedBlockingQueue<Object> queue ;
		
		private Task task ;
		
		public Run(LinkedBlockingQueue<Object> queue , Task task) {
			this.queue = queue ;
			this.task = task ;
		}
		 
		@Override
		public void run() {
			while(true) {
				try {
					Object obj = queue.take();
					task.execute(obj);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
	}
}

