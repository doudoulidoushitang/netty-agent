package com.ygsoft.nagent.config.command;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.ygsoft.nagent.netty.server.NettyServer;

@Component
public class NettyServerCommand implements CommandLineRunner{

	@Override
	public void run(String... args) throws Exception {
		NettyServer server = new NettyServer();
		server.bind();
	}

}
