package com.ygsoft.nagent.netty.server;

import java.util.Arrays;
import java.util.List;

import com.ygsoft.nagent.util.Utils;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

public class NettyServerDecoder extends ByteToMessageDecoder{

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		Object obj = decodeMsg(in);
		if(obj != null) {
			out.add(obj);
		}
	}
	
	private Object decodeMsg(ByteBuf buf) {
		buf.markReaderIndex();
		if(buf.readableBytes() <2) {
			return null ; 
		}
		byte startByte = buf.readByte();
		byte chekeByte = buf.readByte() ;
		while(0x68 != startByte || chekeByte != 0x04) {
			if(buf.readableBytes() >1) {
				startByte = buf.readByte();
				chekeByte = buf.readByte() ;
			}else {
				return null;
			}
		}
		try {
			if(buf.readableBytes() <4) {
				buf.resetReaderIndex();
				return null ; 
			}
			byte[] lengthByte = new byte[4] ;
			buf.readBytes(lengthByte);
			int length = ((((lengthByte[0]&0xFF)<< 24)  | ((lengthByte[1]&0xFF) << 16) | ((lengthByte[2]&0xFF) << 8) | (lengthByte[3]&0xFF) ) & 0xFFFFFFFF);
			System.out.println("解码器收到报文长度:"+Arrays.toString(lengthByte)+" length="+length+"  buf.readableBytes()="+buf.readableBytes());
			if(length < 0 || length >= Integer.MAX_VALUE) {
				return null ;
			}
			byte[] data = new byte[length];
			if(buf.readableBytes() < length) {
				buf.resetReaderIndex();
				return null ;
			}
			buf.readBytes(data);
			return Utils.deSearialMsg(data);
		} catch (Exception e) {
			e.printStackTrace();
//			buf.resetReaderIndex();
			return null ;
		}
	}
	public static void main(String[] args) {
		byte[] bb = new byte[4];
		short length = 308;
		System.out.println("报文长度"+length);
		bb[3] =(byte)(length&0xFF) ;
		bb[2] =(byte)((length >>> 8)&0xFF) ;
		bb[1] =(byte)((length >>> 16)&0xFF) ;
		bb[0] =(byte)((length >>> 24)&0xFF) ;
		
		bb[0] = 0x00 ;
		bb[1] = 0x00 ;
		bb[2] = 24;
		bb[3] = 13 ;
		System.out.println((((bb[0]<< 24)&0xFF)  | ((bb[1] << 16)&0xFF) | (((bb[2]&0xFF) << 8)) | (bb[3]&0xFF) ));
		System.out.println(Integer.MAX_VALUE);
	}
}
