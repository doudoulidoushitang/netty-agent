package com.ygsoft.nagent.netty.server;

import java.net.InetSocketAddress;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class NettyServer {

	private EventLoopGroup bossGroup = new NioEventLoopGroup(1);
	
	private EventLoopGroup workGroup = new NioEventLoopGroup();
	
	public void bind() {
		ServerBootstrap boot = new ServerBootstrap() ;
		try {
			boot.group(bossGroup, workGroup)
			.channel(NioServerSocketChannel.class)
			.option(ChannelOption.CONNECT_TIMEOUT_MILLIS , 3000)
			.childHandler(new ChannelInitializer<Channel>() {
				@Override
				protected void initChannel(Channel channel) throws Exception {
					ChannelPipeline pipel = channel.pipeline() ;
					pipel.addLast(new NettyServerDecoder());
					pipel.addLast(new NettyServerHandler());
				}
			})
			.bind(new InetSocketAddress("10.52.1.116", 13299)).awaitUninterruptibly();
			System.out.println("采集中心已开始监听端口:13299");
		} catch (Exception e) {
			
		}
	}
}
