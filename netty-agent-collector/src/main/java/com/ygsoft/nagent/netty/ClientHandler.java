package com.ygsoft.nagent.netty;

import java.io.Serializable;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class ClientHandler extends SimpleChannelInboundHandler<Serializable>{

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Serializable msg) throws Exception {
		//todo
	}
	
	@Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        System.out.println("链接已建立");
    }

    /**
     * Do nothing by default, sub-classes may override this method.
     */
    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        System.out.println("断开链接");
    }

}
