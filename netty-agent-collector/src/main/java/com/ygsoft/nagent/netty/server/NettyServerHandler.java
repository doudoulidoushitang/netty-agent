package com.ygsoft.nagent.netty.server;

import java.io.Serializable;

import com.ygsoft.nagent.model.MethodModel;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class NettyServerHandler extends SimpleChannelInboundHandler<Serializable>{

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Serializable msg) throws Exception {
		if(msg instanceof MethodModel) {
			System.out.println(ctx.channel().remoteAddress()+"  "+msg.toString());
		}else {
			System.out.println(ctx.channel().remoteAddress()+"  "+msg.toString());
		}
	}
	
	@Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        System.out.println(ctx.channel().remoteAddress()+"已连接到采集中心");
    }

    /**
     * Do nothing by default, sub-classes may override this method.
     */
    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
    	 System.out.println(ctx.channel().remoteAddress()+"与采集中心断开链接！！！");
    }

}
