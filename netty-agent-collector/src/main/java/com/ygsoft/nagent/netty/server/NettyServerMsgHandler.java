package com.ygsoft.nagent.netty.server;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ygsoft.nagent.service.EsMethodService;

@Component
public abstract class NettyServerMsgHandler {

	@Autowired
	private EsMethodService methodService; 
	
	public void dispatcher(Serializable msg) {
		
	}
	
}
