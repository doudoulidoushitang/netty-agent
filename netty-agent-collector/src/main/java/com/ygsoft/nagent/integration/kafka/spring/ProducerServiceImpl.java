package com.ygsoft.nagent.integration.kafka.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Component
public class ProducerServiceImpl{

	@Autowired
	private KafkaTemplate<String, String> template ;
	
	public void sendJson(String topic , String content ) {
		ListenableFuture<SendResult<String, String>> future = template.send(topic, content);
		future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {

			@Override
			public void onSuccess(SendResult<String, String> result) {
				System.out.println("spring 发送 kafka消息 成功："+result);
			}

			@Override
			public void onFailure(Throwable ex) {
				System.out.println("spring 发送 kafka消息 失败："+ex.getMessage());
			}
		});
		
	}
	
}
