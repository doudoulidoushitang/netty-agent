package com.ygsoft.nagent.integration.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ygsoft.nagent.integration.kafka.spring.ProducerServiceImpl;
import com.ygsoft.nagent.integration.kafka.spring.SpringContextHolder;

@SpringBootApplication
public class Main {
	public static void main(String[] args) {
		SpringApplication.run( Main.class, args );
		ProducerServiceImpl service = SpringContextHolder.SPRING_CONTEXT.getBean(ProducerServiceImpl.class);
		service.sendJson("test", "hello spring kafka");
		
	}
}
