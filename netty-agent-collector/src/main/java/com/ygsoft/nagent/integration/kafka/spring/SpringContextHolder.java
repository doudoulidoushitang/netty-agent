package com.ygsoft.nagent.integration.kafka.spring;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

@Service
public class SpringContextHolder implements ApplicationContextAware{

	public static ApplicationContext SPRING_CONTEXT ;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		SpringContextHolder.SPRING_CONTEXT = applicationContext;
	}

	
	
}
