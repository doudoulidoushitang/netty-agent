package com.ygsoft.nagent.integration.kafka.consumer;

import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

public class KafkaConsumerExample {

	public static void main(String[] args) {
		Properties pro = new Properties();
		pro.put("bootstrap.servers", "localhost:9092");
		pro.put("group.id", "test");
		pro.put("enable.auto.commit", "true");
		pro.put("auto.commit.interval.ms", "1000");
		pro.put("session.timeout.ms", "8000");
		pro.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		pro.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		KafkaConsumer<String, String> consumer = new KafkaConsumer<>(pro);
		consumer.subscribe(Arrays.asList("test"));
		while(true) {
			ConsumerRecords<String, String> records = consumer.poll(100);
			for(ConsumerRecord<String, String> record : records) {
				System.out.println(record.offset()+" ----  "+record.key()+" ----- " +record.value());
			}
		}
	}
}
