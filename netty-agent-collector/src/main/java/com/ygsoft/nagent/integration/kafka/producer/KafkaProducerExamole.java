package com.ygsoft.nagent.integration.kafka.producer;

import java.util.Properties;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

public class KafkaProducerExamole {

	public static void main(String[] args) {
		Properties pro = new Properties();
		pro.put("bootstrap.servers", "localhost:9092");
		pro.put("acks", "all");
		pro.put("retries", 0);
		pro.put("batch.size", 1000);
		pro.put("linger.ms", 1);
		pro.put("buffer.memory", 1024*1024*10);
		pro.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		pro.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		Producer<String, String> producer = new KafkaProducer<>(pro);
		for(int i= 0 ; i < 10 ; i ++) {
			producer.send(new ProducerRecord<String, String>("test", "Hello"+i) , new Callback() {
				
				@Override
				public void onCompletion(RecordMetadata metadata, Exception e) {

					if(null != e) {
						System.out.println(e.getMessage());
					}
					System.out.println("回调"+metadata);
				}
			});
			
		}
		producer.close();
		
	}
}
