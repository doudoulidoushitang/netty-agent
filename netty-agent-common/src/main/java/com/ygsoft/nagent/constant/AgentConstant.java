package com.ygsoft.nagent.constant;

public class AgentConstant {

	public static final String AGENT_PROPERTIES_NAME="agent.properties";
	
	public static final String COLLECTOR_IP="collector-ip";
	
	public static final String COLLECTOR_PORT="collector-port";
	
	public static final String methodSep = "---" ;
	
	public static final String ENDINVOKE_SEP="************************end**************************" ;
	
	public static final String STARTINVOKE_SEP="************************start**************************" ;
	
	public static final String TRANCE_ID = "traceid" ;
	
	public static final String SPAN_ID = "spanid";
	
	public static final String PARENT_SPAN = "parentspan" ;
	
	public static final String CLASS_NAME ="classname" ;
	
	public static final String METHOD_NAME="methodname" ;
	
	public static final String START_NAO="startnao" ;
	
	public static final String END_NAO="endnao" ;
	
	public static final String ERROR = "error";
	
	public static final String RESULT = "result"; 
	
	public static final String COST_TIME = "costtime" ;
	
	public static final String LISTENER = "listener" ;
	
	public static final String CLASS_LOADER = "classloader" ;
	
	public static final String METHOD_DESC = "methoddesc" ;
	
	public static final String TARGET = "target" ;
	
	public static final String PARAM = "param" ;
}
