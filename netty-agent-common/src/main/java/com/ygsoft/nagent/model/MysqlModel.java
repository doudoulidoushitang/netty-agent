package com.ygsoft.nagent.model;

import java.io.Serializable;
import java.util.Properties;

public class MysqlModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String host  ;
	
	private String connectTo ;
	
	private String port ;
	
	private String url ; 
	
	private Properties properties ;
	
	private String type ;
	
	private String sql ;
	
	private Long costTime ;

	public Long getCostTime() {
		return costTime;
	}

	public void setCostTime(Long costTime) {
		this.costTime = costTime;
	}

	public String getConnectTo() {
		return connectTo;
	}

	public void setConnectTo(String connectTo) {
		this.connectTo = connectTo;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	@Override
	public String toString() {
		return "MysqlModel [host=" + host + ", connectTo=" + connectTo + ", port=" + port + ", url=" + url
				+ ", properties=" + properties + ", type=" + type + ", sql=" + sql + ", costTime=" + costTime + "]";
	}
}
