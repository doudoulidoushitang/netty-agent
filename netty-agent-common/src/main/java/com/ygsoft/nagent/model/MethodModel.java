package com.ygsoft.nagent.model;

import java.io.Serializable;

public class MethodModel implements Serializable{

	private String traceId ;
	
	private String spanId ;
	
	private String parentSpan ;
	
	private String className ;
	
	private String methodName ;
	
	private String methodDesc ; 
	
	private Object target ; 
	
	public String getMethodDesc() {
		return methodDesc;
	}

	public void setMethodDesc(String methodDesc) {
		this.methodDesc = methodDesc;
	}

	public Object getTarget() {
		return target;
	}

	public void setTarget(Object target) {
		this.target = target;
	}

	private Long startNao ;
	
	private Long endNao ;
	
	private Throwable error ;
	
	private Object result ;
	
	private Long costTime ;

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	public String getSpanId() {
		return spanId;
	}

	public void setSpanId(String spanId) {
		this.spanId = spanId;
	}

	public String getParentSpan() {
		return parentSpan;
	}

	public void setParentSpan(String parentSpan) {
		this.parentSpan = parentSpan;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Long getStartNao() {
		return startNao;
	}

	public Long getCostTime() {
		return costTime;
	}

	public void setCostTime(Long costTime) {
		this.costTime = costTime;
	}

	public void setStartNao(Long startNao) {
		this.startNao = startNao;
	}

	public Long getEndNao() {
		return endNao;
	}

	public void setEndNao(Long endNao) {
		this.endNao = endNao;
	}

	public Throwable getError() {
		return error;
	}

	public void setError(Throwable error) {
		this.error = error;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "MethodModel [traceId=" + traceId + ", spanId=" + spanId + ", parentSpan=" + parentSpan + ", className="
				+ className + ", methodName=" + methodName + ", startNao=" + startNao + ", endNao=" + endNao
				+ ", error=" + error + ", result=" + result + ", costTime=" + costTime + "]";
	}
	
}
