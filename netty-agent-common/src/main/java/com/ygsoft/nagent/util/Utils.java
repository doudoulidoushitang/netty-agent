package com.ygsoft.nagent.util;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.UUID;
import java.util.Vector;

import org.springframework.util.SerializationUtils;

public class Utils {

	public static void printClasses(ClassLoader classLoader) {
		try {
			Class cla = classLoader.getClass() ;
			while(cla != ClassLoader.class) {
				cla = cla.getSuperclass();
			}
			Field field;
			field = cla.getDeclaredField("classes");
			field.setAccessible(true);
			Vector  v = (Vector) field.get(classLoader);
			for(int i = 0 ; i < v.size(); i ++) {
				String className = ((Class)v.get(i)).getName() ;
				System.err.println("------------------->"+((Class)v.get(i)).getClassLoader()+"   "+ ((Class)v.get(i)).getName());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static String printPreByDeep(String pre , int deep ) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0 ; i < deep ; i ++) {
			sb.append("   ");
		}
		sb.append(pre);
		return sb.toString();
	}
	
	public static byte[] serialMsg(Serializable msg) {
		byte[] data = SerializationUtils.serialize(msg);
		byte[] searialData = new byte[data.length+6];
		searialData[0] = (byte)0x68 ;
		searialData[1] = (byte)0x04 ;
		short length = (short)data.length;
		searialData[5] =(byte)(length&0xFF) ;
		searialData[4] =(byte)((length >>> 8)&0xFF) ;
		searialData[3] =(byte)((length >>> 16)&0xFF) ;
		searialData[2] =(byte)((length >>> 24)&0xFF) ;
		System.arraycopy(data, 0, searialData, 6, length);
		return searialData ;
	}
	
	public static Serializable deSearialMsg(byte[] data) {
		return (Serializable)SerializationUtils.deserialize(data);
	}
	
	public static String getRandomUUID() {
		return UUID.randomUUID().toString();
	}
}
