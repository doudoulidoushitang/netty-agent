package com.ygsoft.nagent.config;

public class AttachConfig {

	private String processPid ;
	
	private String spyPath ;
	
	private String corePath ;

	public String getProcessPid() {
		return processPid;
	}

	public void setProcessPid(String processPid) {
		this.processPid = processPid;
	}

	public String getSpyPath() {
		return spyPath;
	}

	public void setSpyPath(String spyPath) {
		this.spyPath = spyPath;
	}

	public String getCorePath() {
		return corePath;
	}

	public void setCorePath(String corePath) {
		this.corePath = corePath;
	}
	
	
}
