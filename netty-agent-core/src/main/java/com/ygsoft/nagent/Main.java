package com.ygsoft.nagent;

import java.util.List;

import com.ygsoft.nagent.config.AttachConfig;

public class Main {

	public static void main(String[] args){
		
		/** pid spypath corepath **/
		AttachConfig config = new AttachConfig();
		config.setProcessPid(args[0]);
		config.setSpyPath(args[1]);
		config.setCorePath(args[1]);
		try {
			final ClassLoader loader = Thread.currentThread().getContextClassLoader();
			final Class<?> vmdClass = loader.loadClass("com.sun.tools.attach.VirtualMachineDescriptor");
			final Class<?> vmClass = loader.loadClass("com.sun.tools.attach.VirtualMachine");
			
			Object attachVmdObj = null;
			for (Object obj : (List<?>) vmClass.getMethod("list", (Class<?>[]) null).invoke(null, (Object[]) null)) {
				if ((vmdClass.getMethod("id", (Class<?>[]) null).invoke(obj, (Object[]) null))
						.equals(Integer.toString(25636))) {
					attachVmdObj = obj;
				}
			}
			Object vmObj = null ; 
			if(attachVmdObj == null) {
				vmObj = vmClass.getMethod("attach", String.class).invoke(attachVmdObj, args[0]) ;
			}else {
				vmObj = vmClass.getMethod("attach", vmdClass).invoke(attachVmdObj, attachVmdObj) ;
			}
			vmClass.getMethod("loadAgent", String.class , String.class).invoke(vmObj, config.getSpyPath() , config.getCorePath());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
