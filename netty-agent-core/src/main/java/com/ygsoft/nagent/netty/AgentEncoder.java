package com.ygsoft.nagent.netty;

import java.io.Serializable;

import com.ygsoft.nagent.util.Utils;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class AgentEncoder extends MessageToByteEncoder<Serializable>{

	@Override
	protected void encode(ChannelHandlerContext ctx, Serializable msg, ByteBuf out) throws Exception {
		byte[] data = Utils.serialMsg(msg);
		if(data != null && data.length > 0) {
			out.writeBytes(data);
		}
	}

}
