package com.ygsoft.nagent.netty;

import java.net.InetSocketAddress;

import org.springframework.util.StringUtils;

import com.ygsoft.nagent.constant.AgentConstant;
import com.ygsoft.nagent.util.PropertiesUtil;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

public abstract class NettyClient {

	private Channel channel ;
	
	private EventLoopGroup group = new NioEventLoopGroup(2) ;
	
	private Channel connect() {
		String ipAddr = PropertiesUtil.getInstance().getAgentPro(AgentConstant.COLLECTOR_IP);
		String portStr = PropertiesUtil.getInstance().getAgentPro(AgentConstant.COLLECTOR_PORT);
		if(StringUtils.isEmpty(ipAddr) || StringUtils.isEmpty(portStr)) {
			System.out.println("properties is not init");
			return null;
		}
		int port = Integer.parseInt(portStr);
		Bootstrap boot = new Bootstrap();
		try {
			boot.group(group)
			.option(ChannelOption.SO_KEEPALIVE, true)
			.option(ChannelOption.SO_TIMEOUT, 1*60*1000)
			.channel(NioSocketChannel.class)
			.remoteAddress(new InetSocketAddress(ipAddr, port))
			.handler(new ChannelInitializer<Channel>() {

				@Override
				protected void initChannel(Channel channel) throws Exception {
					ChannelPipeline pi = channel.pipeline();
					pi.addLast(new AgentEncoder())
					.addLast(new ClientHandler());
				}
			});
			ChannelFuture future = boot.connect().awaitUninterruptibly();
			if(future.isSuccess()) {
				channel = future.channel() ;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return channel ; 
	}
	
	public Channel reconnect() {
		while(channel == null || !channel.isActive()) {
			connect();
			try {
				Thread.sleep(3000);
			} catch (Exception e) {
			}
		}
		return channel ;
	}
	
	public Channel getChannel() {
		return channel ;
	}
	
	
}
