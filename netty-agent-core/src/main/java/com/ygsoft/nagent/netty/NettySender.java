package com.ygsoft.nagent.netty;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Vector;
import java.util.concurrent.LinkedBlockingQueue;

public class NettySender extends NettyClient{

	private NettySender(){
		init();
	}
	
	/**线程池，也充当消息队列***/
//	private ThreadPoolExecutor executor = new ThreadPoolExecutor(3, 3, 2, TimeUnit.MINUTES, new LinkedBlockingQueue<>(200));
	
	private int threadNum = 3 ;
	
	private Thread[] executor ;
	
	private LinkedBlockingQueue<Serializable>[] queues ;
	
	public static NettySender getInstance() {
		return Singleton.INSTANCE;
	}
	
	private void init() {
		System.out.println("开始初始化，线程池大小"+threadNum);
		queues = new LinkedBlockingQueue[3];
		executor = new Thread[threadNum];
		for(int i= 0 ; i < threadNum ; i ++) {
			queues[i] = new LinkedBlockingQueue<Serializable>(500);
			executor[i] = new Thread(new ThreadTask(queues[i]));
			executor[i].start();
			System.out.println(i+"线程已启动");
		}
	}
	
	static class ThreadTask implements Runnable{
		private LinkedBlockingQueue<Serializable> queue; 
		
		public ThreadTask(LinkedBlockingQueue<Serializable> queue) {
			this.queue = queue ;
		}

		@Override
		public void run() {
			while(true) {
				try {
					Serializable msg = queue.take();
					NettySender.getInstance().sendAndFlush(msg);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private static class Singleton{
		public static final NettySender INSTANCE = new NettySender();
	}
	
	private void sendAndFlush(Serializable msg) {
		if(getChannel() ==null || !getChannel().isActive()) {
			reconnect();
		}
		getChannel().writeAndFlush(msg);
	}
	
	public void sendMsg(Serializable msg ) {
		sendMsg(msg , 0);
	}
	
	public void sendMsg(Serializable msg ,int key) {
		int index = 0 ;
		if(key == -1) {
			index = (int)(Math.random()*(threadNum));
		}else {
			index = key%threadNum ;
		}
		//队列满时返回false，当没有连接到采集中心时，会导致消息丢失，应做补偿处理
		queues[index].offer(msg);
	}
	public static void main(String[] args) throws Exception {
	}
}
