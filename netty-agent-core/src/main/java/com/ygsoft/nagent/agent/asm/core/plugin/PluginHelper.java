package com.ygsoft.nagent.agent.asm.core.plugin;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ServiceLoader;
import java.util.Set;

import com.ygsoft.nagent.agent.asm.core.service.IPlugin;

public class PluginHelper {

	public PluginHelper() {
		loadService();
	}
	
	private Set<IPlugin> plugins = new HashSet<>();
	
	public synchronized List<IPlugin> matchClassName(String className) {
		LinkedList<IPlugin> result = new LinkedList<>();
		for(IPlugin plugin : plugins) {
			if(plugin.match(className)) {
				result.add(plugin) ;
			}
		}
		return result;
	}
	
	private void loadService() {
		ClassLoader oldLoader = Thread.currentThread().getContextClassLoader();
		Thread.currentThread().setContextClassLoader(PluginHelper.class.getClassLoader());
		Iterator<IPlugin> ppp = ServiceLoader.load(IPlugin.class).iterator();
		while(ppp.hasNext()) {
			IPlugin plugin = ppp.next();
			plugins.add(plugin);
		}
		System.out.println("类加载器为:"+Thread.currentThread().getContextClassLoader()+"  已获取插件插件："+plugins);
		Thread.currentThread().setContextClassLoader(oldLoader);
	}
	
	public static void main(String[] args) {
		System.out.println(ClassLoader.getSystemClassLoader());
	}
}
