package com.ygsoft.nagent.agent.asm.core;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class MyMethodAdapter extends MethodVisitor{

	private String name ;
	
	private String methodId ;
	
	private String className ;
	
	public MyMethodAdapter(MethodVisitor methodVisitor, String name , String className) {
		super(Opcodes.ASM7 , methodVisitor);
		this.name = name ;
		this.className = className ;
		this.methodId = MyProfiler.methodIdGrower.getAndIncrement()+"";
	}
	
	private void storeMethodCache() {
		MethodCache cache = new MethodCache();
		cache.setClassName(className);
		cache.setMethodName(name);
		MyProfiler.setCache(this.methodId ,cache);
	}
	
	@Override
	public void visitCode() {
		storeMethodCache();
		// 方法执行起始处理.
		this.visitLdcInsn(methodId);
		this.visitMethodInsn(Opcodes.INVOKESTATIC, "com/ygsoft/nagent/agent/asm/core/MyProfiler", 
				"startMethod", "(Ljava/lang/String;)V", false);
		super.visitCode();
	}
	
	@Override
	public void visitInsn(int opcode) {
		switch (opcode) {
		case Opcodes.IRETURN:
		case Opcodes.LRETURN:
		case Opcodes.FRETURN:
		case Opcodes.DRETURN:
		case Opcodes.ARETURN:
		case Opcodes.RETURN:
			this.visitLdcInsn(methodId);
			// 在此进行统一的方法执行完成的处理.
			this.visitMethodInsn(Opcodes.INVOKESTATIC,"com/ygsoft/nagent/agent/asm/core/MyProfiler", 
					"methodEnd", "(Ljava/lang/String;)V", false);
			break;
		case Opcodes.ATHROW:
			// 在此进行统一的方法执行异常的处理.
			this.visitInsn(Opcodes.DUP);
			this.visitLdcInsn(methodId);
			this.visitMethodInsn(Opcodes.INVOKESTATIC, "com/ygsoft/nagent/agent/asm/core/MyProfiler", 
					"methodError","(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/Throwable;", false);
			break;
		default:
			break;
		}
		super.visitInsn(opcode);
	}

}
