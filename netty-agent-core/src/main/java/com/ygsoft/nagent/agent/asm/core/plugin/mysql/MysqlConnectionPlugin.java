package com.ygsoft.nagent.agent.asm.core.plugin.mysql;

import java.util.HashMap;
import java.util.Properties;

import com.ygsoft.nagent.agent.asm.core.service.AbstractEnhancer;
import com.ygsoft.nagent.agent.asm.core.service.AdviceListener;
import com.ygsoft.nagent.constant.AgentConstant;
import com.ygsoft.nagent.model.MysqlModel;
import com.ygsoft.nagent.netty.NettySender;
import com.ygsoft.nagent.util.Utils;

public class MysqlConnectionPlugin extends AbstractEnhancer{

	@Override
	public AdviceListener getListener() {
		return new AdviceListener() {
			
			@Override
			public boolean isIgnore(int access, ClassLoader loader, String className, String methodName) {
				if(methodName.indexOf("getInstance") >=0) {
					System.err.println("增强Mysql connection");
					return false ;
				}
				return true;
			}
			
			@Override
			public void before(HashMap<String, Object> methodInfo) throws Throwable {
				
			}
			
			@Override
			public void afterThrowing(HashMap<String, Object> methodInfo) throws Throwable {
				
			}
			
			@Override
			public void afterReturning(HashMap<String, Object> methodInfo) throws Throwable {
				Object[] args = (Object[]) methodInfo.get(AgentConstant.PARAM);
				MysqlModel model = new MysqlModel() ;
				model.setHost((String)args[0]);
				model.setPort(args[1].toString());
				model.setProperties((Properties)args[2]);
				model.setConnectTo(args[3].toString());
				model.setUrl(args[4].toString());
				model.setType("CREATE_CONNECTION");
				Long startNao = (Long)methodInfo.get(AgentConstant.START_NAO) ;
				Long endNao = (Long)methodInfo.get(AgentConstant.END_NAO) ;
				model.setCostTime(endNao - startNao);
				NettySender.getInstance().sendMsg(model, 1);
			}
		};
	}

	@Override
	public String getMatchString() {
		return "com.mysql.jdbc.ConnectionImpl";
	}

}
