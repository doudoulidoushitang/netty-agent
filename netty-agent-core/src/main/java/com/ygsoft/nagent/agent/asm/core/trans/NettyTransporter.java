package com.ygsoft.nagent.agent.asm.core.trans;

import java.io.Serializable;
import java.util.concurrent.LinkedBlockingQueue;

public class NettyTransporter {

	private LinkedBlockingQueue<Serializable> msgQueue = new LinkedBlockingQueue<>();
	
	private NettyTransporter() {}
	
	private static class Singleton{
		private static NettyTransporter trans = new NettyTransporter();
	}
	
	public NettyTransporter getInstance() {
		return Singleton.trans;
	}
	
	public void start() {
		
	}
}
