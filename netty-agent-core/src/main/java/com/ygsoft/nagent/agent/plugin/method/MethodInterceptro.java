package com.ygsoft.nagent.agent.plugin.method;

import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import com.ygsoft.nagent.model.MethodModel;

import net.bytebuddy.implementation.bind.annotation.AllArguments;
import net.bytebuddy.implementation.bind.annotation.Origin;
import net.bytebuddy.implementation.bind.annotation.RuntimeType;
import net.bytebuddy.implementation.bind.annotation.SuperCall;
import net.bytebuddy.implementation.bind.annotation.This;

public class MethodInterceptro {
	private static ThreadLocal<LinkedList<MethodModel>> stack = new ThreadLocal<LinkedList<MethodModel>>();
	
	private static ThreadLocal<AtomicInteger> stackDeep = new ThreadLocal<>();
	
	private static ConcurrentHashMap<String, LinkedList<MethodModel>> stackMap = new ConcurrentHashMap<>();
	
	@RuntimeType
	public void intercept(@This Object obj ,@AllArguments Object[] allArguments, 
				@Origin Method method,@SuperCall Callable<?> callable) {
		System.out.println("开始探针方法"+obj.getClass().getName()+" "+method.getName());
		if(stack.get() == null) {
			stack.set(new LinkedList<MethodModel>());
		}
		if(stackDeep.get() == null) {
			stackDeep.set(new AtomicInteger(0));
		}
		stackDeep.get().getAndIncrement();
		MethodModel model = packageMethod(obj, method, allArguments);
		try {
			Object result = callable.call();
			model.setResult(result);
		} catch (Exception e) {
			model.setEndNao(System.nanoTime());
			model.setError(e);
		}
		model.setCostTime((System.nanoTime()-model.getStartNao())/1000000);
		stack.get().addFirst(model);
		int deep = stackDeep.get().decrementAndGet();
		System.out.println("探针方法结束"+obj.getClass().getName()+" "+method.getName()+"  deep="+deep);
		//栈深度为0
		if(deep == 0) {
			LinkedList<MethodModel> list =stack.get();
			System.out.println("-------------------------------");
			for(MethodModel mm : list) {
				System.out.println(mm);
			}
			System.out.println("-------------------------------");
			stack.remove();
		}
	}
	
	private MethodModel packageMethod(Object obj ,Method method , Object[] allArguments) {
		MethodModel model = new MethodModel();
//		model.setMethodId(MyProfiler.methodIdGrower.getAndIncrement()+"");
		model.setClassName(obj.getClass().getName());
		model.setStartNao(System.nanoTime());
		model.setMethodName(method.getName());
		return model ;
	}
}
