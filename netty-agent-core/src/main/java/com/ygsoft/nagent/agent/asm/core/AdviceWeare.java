package com.ygsoft.nagent.agent.asm.core;

import java.util.HashMap;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.AdviceAdapter;
import org.objectweb.asm.commons.JSRInlinerAdapter;
import org.objectweb.asm.commons.Method;

import com.ygsoft.nagent.agent.asm.core.service.AdviceListener;
import com.ygsoft.nagent.constant.AgentConstant;
import com.ygsoft.nagent.util.Utils;

public class AdviceWeare extends ClassVisitor implements Opcodes{

	private String className ;
	
	private AdviceListener listener ;
	
	private Class<?> targetClazz ;
	
	private int adviceId ;
	
	private ClassWriter writer ;
	
	private ClassLoader classLoader ; 
	
	/****
	 * 构造方法，传入listener
	 * @param className
	 * @param listener
	 * @param targetClazz
	 * @param adviceId
	 */
	public AdviceWeare(String className , AdviceListener listener , ClassLoader classLoader ,ClassVisitor cv) {
		super(Opcodes.ASM7,cv);
		writer = (ClassWriter) super.cv ;
		this.className = className ; 
		this.listener = listener ;
		this.classLoader = classLoader ;
		this.adviceId = adviceidProducer.getAndIncrement() ;
		LISTENER_MAP.put(this.adviceId, this.listener);
	}

	private static AtomicInteger adviceidProducer = new AtomicInteger(1);
	
	private static final ConcurrentHashMap<Integer, AdviceListener> LISTENER_MAP = new ConcurrentHashMap<>();
	
	private static final ThreadLocal<AtomicInteger> methodDeep = new ThreadLocal<AtomicInteger>() {
		
		@Override
		protected AtomicInteger initialValue() {
			return new AtomicInteger(0) ;
		}
	};
	
	private static final ThreadLocal<Boolean> selfCall = new ThreadLocal<Boolean>() {
		@Override
		protected Boolean initialValue() {
			return false ;
		}
	};
	
	private static final ThreadLocal<Stack<HashMap<String , Object>>> THREAD_STACK = new ThreadLocal<Stack<HashMap<String , Object>>>();
	
	public static void methodOnBefore(int adviceId , ClassLoader classLoader, String className, String methodName, String methodDesc, Object target,
            Object[] args) {
		if(selfCall.get()) {
			return ;
		}
		selfCall.set(true);
		String spanId = Utils.getRandomUUID();
		String traceId  ;
		String parentSpan = null ;
		Stack<HashMap<String , Object>> threadStack = THREAD_STACK.get();
		//为空表示方法未开始，为空表示新的一轮方法开始
		if(threadStack == null || threadStack.size() == 0) {
			threadStack = new Stack<>();
			THREAD_STACK.set(threadStack);
			traceId = Utils.getRandomUUID() ;
		}else {
			HashMap<String, Object> parent = threadStack.peek() ;
			traceId = (String)parent.get(AgentConstant.TRANCE_ID) ;
			parentSpan = (String)parent.get(AgentConstant.PARENT_SPAN); 
		}
		try { 
			AdviceListener listener = LISTENER_MAP.get(adviceId);
			HashMap<String ,Object> methodMap = new HashMap<String ,Object>();
			methodMap.put(AgentConstant.LISTENER, listener);
			methodMap.put(AgentConstant.CLASS_LOADER, classLoader);
			methodMap.put(AgentConstant.CLASS_NAME, className);
			methodMap.put(AgentConstant.METHOD_NAME, methodName);
			methodMap.put(AgentConstant.METHOD_DESC, methodDesc);
			methodMap.put(AgentConstant.TARGET, target);
			methodMap.put(AgentConstant.START_NAO, System.nanoTime());
			methodMap.put(AgentConstant.PARAM, args);
			methodMap.put(AgentConstant.TRANCE_ID, traceId);
			methodMap.put(AgentConstant.SPAN_ID, spanId);
			methodMap.put(AgentConstant.PARENT_SPAN, parentSpan);
			THREAD_STACK.get().push(methodMap);
			if(listener != null) {
				try {
					listener.before(methodMap);
				} catch (Throwable e) {
					//do something
				}
			}
		} finally {
			selfCall.set(false);
		}
	}
	
	 public static void methodOnReturning(Object returnVal) {
	        methodOnEnd(false, returnVal);
	    }

	    public static void methodOnThrowing(Throwable throwable) {
	        methodOnEnd(true, throwable);
	    }

	    public static void methodOnEnd(boolean isThrowing, Object returnTarget) {
	        if (selfCall.get()) {
	            return;
	        } else {
	        	selfCall.set(true);
	        }
	        //从栈中恢复执行现场
	        try {
	            HashMap<String, Object> beginMethodFrame = THREAD_STACK.get().pop();
	            beginMethodFrame.put(AgentConstant.END_NAO, System.nanoTime());
	            AdviceListener listener = (AdviceListener)beginMethodFrame.get(AgentConstant.LISTENER);
	            try {
	            	if (isThrowing) {
	            		beginMethodFrame.put(AgentConstant.ERROR, returnTarget);
		                //执行
	            		listener.afterThrowing(beginMethodFrame);
		            } else {
		            	beginMethodFrame.put(AgentConstant.RESULT, returnTarget);
		            	listener.afterReturning(beginMethodFrame);
		            }
				} catch (Throwable e) {
					//do something
				}
	        } finally {
	            selfCall.set(false);
	        }
	    }
	
	/***
	 * 方法权限
	 * 方法名，
	 * 方法描述
	 * 方法签名
	 * throws
	 */
	@Override
	public MethodVisitor visitMethod(final int access, final String name, final String descriptor, 
			final String signature, final String[] exceptions) {
		MethodVisitor methodVisitor = super.visitMethod(access, name, descriptor, signature, exceptions);
		if(methodVisitor == null ) {
			return null ; 
		}
		if(listener.isIgnore(access, classLoader , className , name)) {
			return methodVisitor ;
		}
		return new AdviceAdapter(ASM5, new JSRInlinerAdapter(methodVisitor, access, name, descriptor, signature, exceptions),
	            access, name, descriptor) {
			
			private Label startLable = new Label() ;
			
			private Label endLable = new Label();
			
			@Override
			protected void onMethodEnter() {
				/**获取spy的bengin方法**/
				getStatic(Type.getType("Lcom/dnetty/agent/Spy;"), "ON_BEFORE_METHOD", Type.getType(java.lang.reflect.Method.class));
				// 推入Method.invoke()的第一个参数
                push((Type)null);
				
                loadBeginMethodArgs();
				invokeVirtual(Type.getType(java.lang.reflect.Method.class), 
						Method.getMethod("Object invoke(Object,Object[])"));
				invokeStatic(Type.getType(AdviceWeare.class), MethodFinder.getAsmMethod(AdviceWeare.class, "methodOnBefore",
						int.class, ClassLoader.class, String.class, String.class, String.class, Object.class,
			            Object[].class));
				pop();
				mark(startLable);
			}
			
			private void loadBeginMethodArgs() {
				//入栈参数长度
				push(7);
				//创建数组
				newArray(Type.getType(Object.class));
				
				dup();
				push(0);
				/**将adviceid压入堆栈***/
				push(adviceId);
				/**将adviceid转换为int**/
				box(Type.getType(int.class));
				arrayStore(Type.getType(Integer.class));
				
				dup();
				push(1);
				loadClassLoader();
				arrayStore(Type.getType(ClassLoader.class));
				
				dup();
				push(2);
				push(className);
				arrayStore(Type.getType(String.class));
				
				dup();
				push(3);
				push(name);
				arrayStore(Type.getType(String.class));
				
				dup();
				push(4);
				push(descriptor);
				arrayStore(Type.getType(String.class));
				
				dup();
				push(5);
				if((ACC_STATIC & methodAccess) == ACC_STATIC) {
					push((Type)null);
				}else {
					loadThis();
				}
				arrayStore(Type.getType(Object.class));
				
				dup();
				push(6);
				loadArgArray();
				arrayStore(Type.getType(Object[].class));
			}
			
			@Override
			protected void onMethodExit(final int opcode) {
				 //判断不是以一场结束
                if (ATHROW != opcode) {
                	//加载正常的返回值
                	loadReturn(opcode);
                	getStatic(Type.getType("Lcom/dnetty/agent/Spy;"), "ON_RETURN_METHOD", Type.getType(java.lang.reflect.Method.class));
                	push((Type)null);
                	loadReturnArgs();
                    invokeVirtual(Type.getType(java.lang.reflect.Method.class), Method.getMethod("Object invoke(Object,Object[])"));
                    pop();
                    //只有一个参数就是返回值
//                    invokeStatic(Type.getType(AdviceWeare.class), MethodFinder.getAsmMethod(AdviceWeare.class, "methodOnReturning",	Object.class));
                }
			}
			
			private void loadReturnArgs() {
				dup2X1();
                pop2();
                push(1);
                newArray(Type.getType(Object.class));
                dup();
                dup2X1();
                pop2();
                push(0);
                swap();
                arrayStore(Type.getType(Object.class));
			}
			
			@Override
	        public void visitMaxs(int maxStack, int maxLocals) {
				//每个方法最后调用一次,在visitEnd之前
                mark(endLable);
                //在beginLabel和endLabel之间使用try-catch block,在这之后需要紧跟exception的处理逻辑code
                visitTryCatchBlock(startLable, endLable, mark(),
                        Type.getType(Throwable.class).getInternalName());
                //从栈顶加载异常(复制一份给onThrowing当参数用)
                dup();
                getStatic(Type.getType("Lcom/dnetty/agent/Spy;"), "ON_THROWS_METHOD", Type.getType(java.lang.reflect.Method.class));
                
                push((Type)null);
                loadThrowArgs();
                invokeVirtual(Type.getType(java.lang.reflect.Method.class), Method.getMethod("Object invoke(Object,Object[])"));
                pop();
//              invokeStatic(Type.getType(AdviceWeare.class), MethodFinder.getAsmMethod(AdviceWeare.class, "methodOnThrowing",	Throwable.class));
                //将原有的异常抛出(不破坏原有异常逻辑)
                throwException();
                super.visitMaxs(maxStack, maxLocals);
			}
			 
            /****创建throwing通知参数本地变量**/
            private void loadThrowArgs() {
                dup2X1();
                pop2();
                push(1);
                newArray(Type.getType(Object.class));
                dup();
                dup2X1();
                pop2();
                push(0);
                swap();
                arrayStore(Type.getType(Throwable.class));
            }
			
			private void loadClassLoader() {
				if((ACC_STATIC & methodAccess) == ACC_STATIC) {
					visitLdcInsn(className.replaceAll("/", "."));
					invokeStatic(Type.getType(Class.class), MethodFinder.getAsmMethod(Class.class, "forName", String.class));
				}else {
					loadThis();
					invokeVirtual(Type.getType(Object.class), MethodFinder.getAsmMethod(Object.class, "getClass"));
				}
				invokeVirtual(Type.getType(Class.class), MethodFinder.getAsmMethod(Class.class, "getClassLoader"));
			}
			
			private void loadReturn(int opcode) {
                switch (opcode) {

                    case RETURN: {
                        push((Type)null);
                        break;
                    }

                    case ARETURN: {
                        dup();
                        break;
                    }

                    case LRETURN:
                    case DRETURN: {
                        dup2();
                        box(Type.getReturnType(methodDesc));
                        break;
                    }

                    default: {
                        dup();
                        box(Type.getReturnType(methodDesc));
                        break;
                    }

                }
            }
			
		};
	}
	static class MethodFinder {

        private MethodFinder() {

        }

        static Method getAsmMethod(final Class<?> clazz, final String methodName, final Class<?>... parameterTypes) {
            return Method.getMethod(getJavaMethodUnsafe(clazz, methodName, parameterTypes));
        }

        static java.lang.reflect.Method getJavaMethodUnsafe(final Class<?> clazz, final String methodName,
                                                            final Class<?>... parameterTypes) {
            try {
                return clazz.getDeclaredMethod(methodName, parameterTypes);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        }
    }
	public ClassWriter getWriter() {
		return writer;
	}

	public void setWriter(ClassWriter writer) {
		this.writer = writer;
	}
	public static void main(String[] args) {
		System.out.println();
		
	}
	
}
