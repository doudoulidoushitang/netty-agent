package com.ygsoft.nagent.agent.asm.core.plugin;

import java.util.HashMap;

import org.objectweb.asm.Opcodes;

import com.ygsoft.nagent.agent.asm.core.service.AbstractEnhancer;
import com.ygsoft.nagent.agent.asm.core.service.AdviceListener;
import com.ygsoft.nagent.constant.AgentConstant;
import com.ygsoft.nagent.model.MethodModel;
import com.ygsoft.nagent.netty.NettySender;
import com.ygsoft.nagent.util.Utils;

public class BaseMethodTimePlugin extends AbstractEnhancer{

	@Override
	public String getMatchString() {
//		return "com.ygsoft.ecp.mapp.apmdigital.";
		return "com.sun.ygsoft.ecp.apm.test.";
	}

	@Override
	public AdviceListener getListener() {
		return new AdviceListener() {
			
			@Override
			public boolean isIgnore(int access,ClassLoader loader , String className, String methodName) {
				if((Opcodes.ACC_ABSTRACT & access) ==Opcodes. ACC_ABSTRACT
			            || (Opcodes.ACC_FINAL & access) == Opcodes.ACC_FINAL
			            || "<clinit>".equals(methodName)
			            || "<init>".equals(methodName)
						|| "main".equals(methodName)) {
					return true ;
				}
				if(loader == null) {
					return true ;
				}else {
					return !loader.equals(ClassLoader.getSystemClassLoader());
				}
			}
			
			@Override
			public void before(HashMap<String, Object> methodInfo) throws Throwable {
				/*if(deep == 1) {
					NettySender.getInstance().sendMsg(AgentConstant.STARTINVOKE_SEP , 0);	
				}
				NettySender.getInstance().sendMsg(Utils.printPreByDeep(AgentConstant.methodSep, deep)+methodName+"("+Arrays.toString(args)+")", 0);*/
			}
			
			@Override
			public void afterThrowing(HashMap<String, Object> methodInfo) throws Throwable {
				NettySender.getInstance().sendMsg(packageMethod(methodInfo));
			}
			
			@Override
			public void afterReturning(HashMap<String, Object> methodInfo) throws Throwable {
				NettySender.getInstance().sendMsg(packageMethod(methodInfo));
			}
			
			private MethodModel packageMethod(HashMap<String, Object> methodInfo) {
				MethodModel method = new MethodModel();
		        String methodDesc = (String)methodInfo.get(AgentConstant.METHOD_DESC);
		        String methodName = (String)methodInfo.get(AgentConstant.METHOD_NAME);
		        String className = (String)methodInfo.get(AgentConstant.CLASS_NAME);
		        method.setMethodName(methodName);
		        method.setClassName(className);
		        method.setMethodDesc(methodDesc);
		        method.setTraceId((String)methodInfo.get(AgentConstant.TRANCE_ID));
		        method.setSpanId((String)methodInfo.get(AgentConstant.SPAN_ID));
		        method.setParentSpan((String)methodInfo.get(AgentConstant.PARENT_SPAN));
		        Long startNao = (Long)methodInfo.get(AgentConstant.START_NAO) ;
				Long endNao = (Long)methodInfo.get(AgentConstant.END_NAO) ;
				method.setCostTime(endNao - startNao);
				method.setStartNao((Long)methodInfo.get(AgentConstant.START_NAO));
				method.setEndNao((Long)methodInfo.get(AgentConstant.END_NAO));
				method.setError((Throwable)methodInfo.get(AgentConstant.ERROR));
				method.setResult((Object)methodInfo.get(AgentConstant.RESULT));
				return method; 
			}
		};
	}

}
