package com.ygsoft.nagent.agent.plugin.method;

public class MyOriDemoImp {

	private String name ;
	
	public MyOriDemoImp(String name) {
		this.name = name ;
	}
	
	public String sayHello() throws Throwable{
		printMsg("这是子方法1");
		System.out.println("start sayHello method ori");
		System.out.println("hello "+name);
		System.out.println("end sayHello method ori");
		printMsg("这是子方法2");
		/*try {
			throwError();
		} catch (Throwable e) {
			throw e ;
		}*/
		return "hello"+name ;
	}

	private void printMsg(String msg) {
		try {
			Thread.sleep(1000L);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(msg);
	}
	private void throwError()  throws Throwable{
		Integer.parseInt(name);
	} 
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
