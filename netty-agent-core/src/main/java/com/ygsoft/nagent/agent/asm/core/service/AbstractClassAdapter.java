package com.ygsoft.nagent.agent.asm.core.service;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;

public abstract class AbstractClassAdapter extends ClassVisitor{

	private ClassWriter classWriter ;
	
	public AbstractClassAdapter() {
		super(Opcodes.ASM7,new ClassWriter(ClassWriter.COMPUTE_MAXS));
		this.classWriter = (ClassWriter) cv ;
	}

	public ClassWriter getClassWriter() {
		return classWriter;
	}

	public void setClassWriter(ClassWriter classWriter) {
		this.classWriter = classWriter;
	}

}
