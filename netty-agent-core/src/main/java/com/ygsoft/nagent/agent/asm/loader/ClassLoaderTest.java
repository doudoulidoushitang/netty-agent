package com.ygsoft.nagent.agent.asm.loader;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;

public class ClassLoaderTest {

	private static Method method = null ; 
	public static void main(String[] args) throws Exception {
		/*System.out.println("----------------------start URLClassLoader-----------------------");
		File file = new File("C:\\workspace\\apm-agent\\netty-agent\\","nett-agent.jar");
		
		ClassLoader agentLoader = new NettyAgentClassLoader(new URL[] {file.toURI().toURL()}) ;
		Class<?> profile = agentLoader.loadClass("com.ygsoft.nagent.agent.asm.core.MyProfiler");
		System.out.println(profile);
		System.out.println("----------------------end URLClassLoader-----------------------");*/
		
		
		
		
		DiskClassLoader diskLoader = new DiskClassLoader("D:\\data\\classes\\com\\sun\\ygsoft\\ecp\\apm\\test");
		
		System.out.println(diskLoader.getParent());
		Class cls1 = null ;
		try {
			cls1 = diskLoader.loadClass("com.sun.ygsoft.ecp.apm.test.MyOriDemoImp");
			if(cls1 != null) {
//				Object obj = cls1.newInstance();
				Constructor cons = cls1.getConstructor(String.class);
				Object obj = cons.newInstance("dengwei");
				method = cls1.getDeclaredMethod("sayHello", String.class);
				method.invoke(obj, "hello");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		DiskClassLoader diskLoader2 = new DiskClassLoader("D:\\data\\lib");
		System.out.println(" Thread "+Thread.currentThread().getName() +" classLoader = "+Thread.currentThread().getContextClassLoader() );
		Thread.currentThread().setContextClassLoader(diskLoader2);
		new Thread(()-> {
			System.out.println(" Thread "+Thread.currentThread().getName() +" classLoader = "+Thread.currentThread().getContextClassLoader() );
			try {
				ClassLoader loader = Thread.currentThread().getContextClassLoader();
				Class c = loader.loadClass("com.dengwei.clazzloader.SpeakTest");
				System.out.println(c.getClassLoader().toString());
				if(c != null) {
					try {
						Object obj = c.newInstance();
						Method method = c.getDeclaredMethod("speak", null);
						method.invoke(obj, null);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}) .start();
		
	}
	
	public static void test1() {
		DiskClassLoader diskLoader = new DiskClassLoader("D:\\data\\lib") ;
		try {
//			Thread.currentThread().setContextClassLoader(diskLoader);
			Class c = diskLoader.loadClass("com.dengwei.clazzloader.Test");
			if(c != null) {
				try {
					Object obj = c.newInstance();
					Method method = c.getDeclaredMethod("say", null);
					method.invoke(obj, null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
