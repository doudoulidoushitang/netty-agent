package com.ygsoft.nagent.agent;

import static net.bytebuddy.matcher.ElementMatchers.isStatic;
import static net.bytebuddy.matcher.ElementMatchers.nameStartsWith;
import static net.bytebuddy.matcher.ElementMatchers.not;

import java.io.File;
import java.io.IOException;
import java.lang.instrument.Instrumentation;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.jar.JarFile;

import com.ygsoft.nagent.agent.asm.NettyAgentClassLoader;
import com.ygsoft.nagent.agent.plugin.method.MethodInterceptro;

import net.bytebuddy.agent.builder.AgentBuilder;
import net.bytebuddy.description.NamedElement;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.dynamic.DynamicType.Builder;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.matcher.ElementMatcher;
import net.bytebuddy.matcher.ElementMatchers;
import net.bytebuddy.utility.JavaModule;

public class AgentMain {

	private static String spyPath = "C:\\workspace\\apm-agent\\netty-agent";
	private static volatile ClassLoader nettyAgentClassLoader ;
	
	public static void premain(final String args, final Instrumentation inst) {
		try {
//			byteBuddyMain(args, inst);
			asmMain(args, inst);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void asmMain(String args , Instrumentation inst) throws Exception{
		File spyJar = new File(spyPath , "arthas-spy.jar");
		if(!spyJar.exists()) {
			throw new Exception("sypjar is not exist ...");
		}
		File agentJar = new File(spyPath , "netty-agent.jar");
		System.out.println(agentJar.getAbsolutePath());
		if(!agentJar.exists()) {
			throw new Exception("agentJar is not exist ...");
		}
		ClassLoader classLoader = getClassLoader(inst, spyJar, agentJar);
		initSpy(classLoader);
//		inst.addTransformer(new MyTransFormer(new PluginHelper()));
	}
	
	private static void initSpy(ClassLoader classLoader) throws ClassNotFoundException, NoSuchMethodException, SecurityException {
		Class<?> adviceClazz = classLoader.loadClass("com.ygsoft.nagent.agent.asm.core.AdviceWeare");
		Method startMethod = adviceClazz.getDeclaredMethod("methodOnBefore", int.class, ClassLoader.class, String.class, String.class, String.class, Object.class,
	            Object[].class);
		Method endMethod = adviceClazz.getDeclaredMethod("methodOnReturning", Object.class);
		Method errorMethod = adviceClazz.getDeclaredMethod("methodOnThrowing", Throwable.class);
	}
	
	private static ClassLoader getClassLoader(Instrumentation inst , File spyJar , File agentJar) throws IOException {
		inst.appendToBootstrapClassLoaderSearch(new JarFile(spyJar));
		return initordefineClassLoader(agentJar);
	}
	
	private static ClassLoader initordefineClassLoader(File agentJar) throws MalformedURLException {
		if(nettyAgentClassLoader == null) {
			nettyAgentClassLoader = new NettyAgentClassLoader(new URL[] {agentJar.toURI().toURL()});
		}
		return nettyAgentClassLoader ;
	}
	
	private static void byteBuddyMain(String args , Instrumentation inst) {
		System.out.println("开始执行main");
		/*new AgentBuilder.Default()
        .ignore(
            nameStartsWith("net.bytebuddy.")
            .or(nameStartsWith("org.slf4j."))
            .or(nameStartsWith("org.apache.logging."))
            .or(nameStartsWith("org.groovy."))
            .or(nameContains("javassist"))
            .or(nameContains(".asm."))
            .or(nameStartsWith("sun.reflect"))
            .or(nameStartsWith("java.lang."))
            .or(nameStartsWith("sun."))
            .or(nameContains("com.ygsoft.nagent.agent.AgentMain"))
//            .or(nettAgentPackage())
            .or(ElementMatchers.<TypeDescription>isSynthetic()))
        .type(any()) //需要增强的类
        .transform(new Transformer() {
			@Override
			public Builder<?> transform(Builder<?> builder, TypeDescription typeDescription, ClassLoader classLoader,
					JavaModule module) {
				System.out.println("开始增强"+typeDescription);
				return builder.method(not(ElementMatchers.isGetter())
						.and(not(ElementMatchers.isSetter()))).intercept(MethodDelegation.to(new MethodInterceptro()));
			}
		})
        .installOn(inst);*/
		
		new AgentBuilder.Default()
        .type(not(ElementMatchers.isInterface()))
        .transform(new AgentBuilder.Transformer() {
            @Override
            public Builder<?> transform(Builder<?> builder, TypeDescription typeDescription, ClassLoader classLoader, JavaModule module) {
            	System.out.println(typeDescription.getActualName());
            	return builder.method(not(isStatic()))
                    .intercept(MethodDelegation.to(new MethodInterceptro()));
            }
        }).installOn(inst);
	}
	
	public static ClassLoader agentLoader() {
		return nettyAgentClassLoader ;
	}
	
	private static ElementMatcher.Junction<NamedElement> nettAgentPackage(){
		return nameStartsWith("com.ygsoft.nagent");
	}
}
