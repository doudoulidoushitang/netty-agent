package com.dnetty.agent;

import java.net.URL;
import java.net.URLClassLoader;

public class NettyAgentClassLoader extends URLClassLoader{

	public NettyAgentClassLoader(URL[] urls) {
		super(urls , ClassLoader.getSystemClassLoader().getParent());
	}

	@Override
    protected synchronized Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        final Class<?> loadedClass = findLoadedClass(name);
        if (loadedClass != null) {
            return loadedClass;
        }
        // 优先从parent（SystemClassLoader）里加载系统类，避免抛出ClassNotFoundException
        if (name != null && (name.startsWith("sun.") || name.startsWith("java.")|| name.startsWith("javax."))) {
            return super.loadClass(name, resolve);
        }
        try {
        	System.out.println("自定义加载器开始加载类："+name);
            Class<?> aClass = findClass(name);
            if (resolve) {
                resolveClass(aClass);
            }
            return aClass;
        } catch (Exception e) {
           e.printStackTrace();
        }
        return super.loadClass(name, resolve);
    }
}
