package com.dnetty.agent;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarFile;

public class AgentMain {

	private static String spyPath = "C:\\workspace\\apm-agent\\netty-agent-core\\target";
	private static volatile ClassLoader nettyAgentClassLoader ;
	
	public static void premain(final String args, final Instrumentation inst) {
		try {
//			byteBuddyMain(args, inst);
			asmMain(args, inst);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void asmMain(String args , Instrumentation inst) throws Exception{
		/*File spyJar = new File(spyPath , "arthas-spy.jar");
		if(!spyJar.exists()) {
			throw new Exception("sypjar is not exist ...");
		}*/
		File agentJar = new File(spyPath , "netty-agent-0.0.1-SNAPSHOT-jar-with-dependencies.jar");
		System.out.println(agentJar.getAbsolutePath());
		if(!agentJar.exists()) {
			throw new Exception("agentJar is not exist ...");
		}
		inst.appendToBootstrapClassLoaderSearch(new JarFile(AgentMain.class.getProtectionDomain().getCodeSource().getLocation().getFile()));
		ClassLoader classLoader = getClassLoader(inst, agentJar);
//		initTransport(classLoader);
		initSpy(classLoader);
		/*Class<?> pluginHelperClass = classLoader.loadClass("com.ygsoft.nagent.agent.asm.core.plugin.PluginHelper");
		Object pluginHelper = pluginHelperClass.newInstance() ;
		System.out.println("pluginHelper="+pluginHelper);*/
		Class<?> transClass = classLoader.loadClass("com.ygsoft.nagent.agent.asm.core.MyTransFormer");
		ClassFileTransformer trans = (ClassFileTransformer)transClass.newInstance();
		inst.addTransformer(trans);
		
	}
	
	private static void initTransport(ClassLoader classLoader) throws ClassNotFoundException, NoSuchMethodException, SecurityException {
		Class<?> senderClass = classLoader.loadClass("com.ygsoft.nagent.netty.NettySender");
		Method send = senderClass.getDeclaredMethod("SEND_MSG", Serializable.class);
		Spy.setSender(send);
	}
	
	private static void initSpy(ClassLoader classLoader) throws ClassNotFoundException, NoSuchMethodException, SecurityException {
		Class<?> adviceClazz = classLoader.loadClass("com.ygsoft.nagent.agent.asm.core.AdviceWeare");
		Method startMethod = adviceClazz.getDeclaredMethod("methodOnBefore", int.class, ClassLoader.class, String.class, String.class, String.class, Object.class,
	            Object[].class);
		Method endMethod = adviceClazz.getDeclaredMethod("methodOnReturning", Object.class);
		Method errorMethod = adviceClazz.getDeclaredMethod("methodOnThrowing", Throwable.class);
		Spy.init(classLoader, startMethod, endMethod, errorMethod);
	}
	
	private static ClassLoader getClassLoader(Instrumentation inst , File agentJar) throws IOException {
//		inst.appendToBootstrapClassLoaderSearch(new JarFile(spyJar));
		return initordefineClassLoader(agentJar);
	}
	
	private static ClassLoader initordefineClassLoader(File agentJar) throws MalformedURLException {
		if(nettyAgentClassLoader == null) {
			nettyAgentClassLoader = new NettyAgentClassLoader(new URL[] {agentJar.toURI().toURL()});
		}
		return nettyAgentClassLoader ;
	}
	
	public static ClassLoader agentLoader() {
		return nettyAgentClassLoader ;
	}
	
	public static void printeInfometion(Instrumentation inst) {
		Class[] allClass = inst.getAllLoadedClasses();
		Set<Class<?>> matched = new HashSet<Class<?>>();
		for(Class<?> clazz :allClass) {
		}
	}
	
}
